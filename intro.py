import pygame
import copy
import random

WIDTH = 1000
HEIGHT = 800

music.set_volume(0.5)
music.play('gamekar')

# TODO:
# Add ruler to GameBoard
# Move gameboard collision to GameBoard class instead of Shape.

def get_rect_list(bricks):
    '''
    Helper method to compile a list of rects from a list of bricks
    '''
    rect_list = []
    for brick in bricks:
        rect_list.extend(brick.get_rect())
    return rect_list

def grid_list(block_size):
    '''
    Generates a gridlist used shape class
    '''
    grids = []
    for y in range(-5, 1):
        row = []
        for x in range(0, 6):
            row.append((x * block_size, y * block_size))
        grids.append(row)

    return grids

class Shape(object):
    '''
    This will make the foundation for a Shape that the others shapes
    will inherit.
    '''
    def __init__(self, x_pos, size):
        '''
        Inits the shape to start at y_pos and every block is of size in size.
        OShape consists of 4 blocks, each block is size wide and high.
        '''
        # the widths and heights is determined by the shape and block size.
        self.block_size = size
        self._width = size
        self._height = size

        # Rows of blocks.
        # _block_init() sets allocates all the rectangles and resets the shape
        # to start position.
        self._x = x_pos - (self.block_size * 3)
        self._y = 0
        self.piece = []

        self.grid = grid_list(self.block_size)

    def shape_grid_update(self):
        '''
        Update shapes position with the given _x and _y value.
        To be used after move or rotation is performed on the shape.
        '''
        for row_n, row in enumerate(self.piece):
            for element_n, element in enumerate(row):
                if element is not None:
                    start_x, start_y = self.grid[row_n][element_n]
                    self.piece[row_n][element_n].x = start_x + self._x
                    self.piece[row_n][element_n].y = start_y + self._y

    def draw(self):
        '''
        Draws all the blocks inside the shape.
        '''
        # screen.draw.filled_rect(self._piece, (0, 255, 0))
        for row in self.piece:
            for element in row:
                if element is not None:
                    screen.draw.filled_rect(element, (0x0, 0xFF, 0x0))

    def pos_rotate(self):
        '''
        Rotate shape clockwise
        '''
        # Transpose the list
        transposed = [new_row for new_row in zip(*self.piece)]

        # Reverse elements in each row.
        rotated_shape = []
        for row in transposed:
            reversed_row = [element for element in reversed(row)]
            rotated_shape.append(reversed_row)

        self.piece = rotated_shape
        self.shape_grid_update()

    def neg_rotate(self):
        '''
        Rotate shape clockwise
        '''
        # Transpose the list
        transposed = [new_row for new_row in zip(*self.piece)]

        # Reverse elements in each row.
        rotated_shape = []
        for row in reversed(transposed):
            rotated_shape.append(row)

        self.piece = rotated_shape
        self.shape_grid_update()

    def move_down(self, speed):
        '''
        Moves the shape downwards in the speed passed in arg speed.
        speed is the number of pixels to move per update.
        '''
        self._y += speed
        self.shape_grid_update()

    def move_up(self, speed):
        '''
        Moves the shape upwards in the speed passed in arg speed.
        speed is the number of pixels to move per update.
        '''
        self._y -= speed
        self.shape_grid_update()

    def move_right(self):
        '''
        Moves the shape 1 step to the right.
        '''
        self._x += self.block_size
        self.shape_grid_update()

    def move_left(self):
        '''
        Moves the shape 1 step to the left
        '''
        self._x -= self.block_size
        self.shape_grid_update()

    def bottom(self):
        '''
        Gives the bottom pos of the first block in the first row.
        '''
        return self._y

    def left(self):
        '''
        Gives the left pos of the first block in the first row.
        '''
        x_pos = []
        for row in self.piece:
            for element in row:
                if element is not None:
                    x_pos.append(element.x)

        return min(x_pos)

    def right(self):
        '''
        Gives the right pos of the last block in the first row.
        '''
        x_pos = []
        for row in self.piece:
            for element in row:
                if element is not None:
                    x_pos.append(element.x)

        return max(x_pos)

    def top(self):
        '''
        Gives the top pos of the first block in the second row.
        '''
        # second row, first block will work fine.
        return self._y + (self.block_size * 2)

    def gameboard_collision(self, gameboard):
        '''
        Check if this piece have collided with any other piece on the bottom.
        '''
        for row in self.piece:
            for element in row:
                if element is not None:
                    if not gameboard.contains(element):
                        if element.bottom > gameboard.bottom:
                            return True
                        elif self.right() >= gameboard.right:
                            return True
                        elif self.left() < gameboard.left:
                            return True
        return False

    def shape_collision(self, other_pieces):
        '''
        Check if this piece have collided with any other piece on the bottom.
        '''

        # Unexpected behaviour. As it seems the bottom check is unnecessary
        for row in self.piece:
            for element in row:
                if element is not None:
                    collisions = element.collidelistall(other_pieces)
                    if collisions:
                        return True
        return False

    def get_rect(self):
        '''
        Returns a list of rects
        '''
        rect_list = []
        for row in self.piece:
            for element in row:
                if element is not None:
                    rect_list.append(element)
                    # rect_list.extend(row_rects)

        return rect_list

    def remove(self, list):
        '''
        Compare list of bricks against piece. If any match remove the entity.
        '''
        for block in list:
            for row in self.piece:
                try:
                    row.remove(block)
                except ValueError:
                    # Expected for some blocks.
                    # Not all blocks resides inside all bricks.
                    pass

    def __str__(self):
        return str(self.piece)


class OShape(Shape):
    def __init__(self, x_pos, size):
        Shape.__init__(self, x_pos, size)
        self.piece = [[None, None, None, None, None, None],
                 [None, None, None, None, None, None],
                 [None, None,
                  Rect((0, 0), (size, size)),
                  Rect((0, 0), (size, size)),
                  None, None],
                 [None, None,
                  Rect((0, 0), (size, size)),
                  Rect((0, 0), (size, size)),
                  None, None],
                 [None, None, None, None, None, None],
                 [None, None, None, None, None, None]]

        # Upate shape with set _x and _y values
        self.shape_grid_update()

    def pos_rotate(self):
        '''
        Try to rotate this one to victory is futile
        '''
        # This will override the method defined in the base class Shape
        pass

    def neg_rotate(self):
        '''
        Try to rotate this one to victory is futile
        '''
        # This will override the method defined in the base class Shape
        pass


class LShape(Shape):
    def __init__(self, x_pos, size):
        Shape.__init__(self, x_pos, size)
        self.piece = [[None, None, None, None, None, None],
                 [None, None,
                  Rect((0, 0), (size, size)),
                  None, None, None],
                 [None, None,
                  Rect((0, 0), (size, size)),
                  None, None, None],
                 [None, None,
                  Rect((0, 0), (size, size)),
                  None, None, None],
                 [None, None,
                  Rect((0, 0), (size, size)),
                  Rect((0, 0), (size, size)),
                  None, None],
                 [None, None, None, None, None, None]]

        # Upate shape with set _x and _y values
        self.shape_grid_update()


class Brick(object):
    '''
    There is the following kind of bricks
    1: O-shaped
    2: L-shaped
    3: I-shaped
    4: Z-shaped

    The Brick class will function as a bridge for all the shapes.
    '''
    def __init__(self, shape, pos, size):
        '''
        Valid data for parameter shape is:
        1, 2, 3, 4
        '''
        self.is_locked = False

        shape_list = {1: OShape, 2: LShape}

        # Check if parameter shape is valid else create the Brick piece.
        if shape not in shape_list:
            raise ValueError('Not in shape_list: {0}'.format(shape))
        else:
            self.piece = shape_list[shape](pos, size)

    @property
    def bottom(self):
        return self.piece.bottom()

    def move_down(self, speed):
        self.piece.move_down(speed)

    def move_up(self, speed):
        self.piece.move_up(speed)

    def move_right(self):
        self.piece.move_right()

    def move_left(self):
        self.piece.move_left()

    def pos_rotate(self):
        self.piece.pos_rotate()

    def neg_rotate(self):
        self.piece.neg_rotate()

    def draw(self):
        '''
        Draw the Brick on to the screen
        '''
        self.piece.draw()

    def gameboard_collision(self, gameboard):
        '''
        Check if Brick have hit gameboard bottom
        '''
        return self.piece.gameboard_collision(gameboard)

    def shape_collision(self, other_pieces):
        '''
        other_pieces is expected to be a list of type Rect
        If piece have collided with other piece at bottom.
        returns true.
        '''
        return self.piece.shape_collision(other_pieces)

    def remove(self, list):
        '''
        If brick contains any of the bricks in list. It will remove them
        '''
        self.piece.remove(list)

    def get_rect(self):
        return self.piece.get_rect()

    def __str__(self):
        return str(self.piece)


class GameBoard(object):
    '''
    Implements the play area and is judge that decides what rectangles
    that should be removed and which dead bricks that should move down.
    '''
    def __init__(self, position, gameboard_size, block_size, color):
        pos_x, _ = position
        self._pos_x = pos_x
        self.cols, self.rows = gameboard_size
        self.block_size = block_size
        width = self.cols * self.block_size
        height = self.rows * self.block_size
        self._board = Rect(position, (width, height))
        self.color = color

    def center(self):
        '''
        Returns the centrum pos in the X axis.
        '''
        center = (self._board.width / 2) + self._pos_x
        return center

    def get_rect(self):
        '''
        Returns a Rect object of the GameBoard
        '''
        return self._board

    def draw(self):
        '''
        To be called when we want it to be drawn onto the screen surface.
        '''
        screen.draw.filled_rect(self._board, self.color)

    def ruler(self, bricks):
        '''
        Takes a list of bricks. Returns a list of rects that should be removed
        from the first row that it encounters
        and a list of rects that should be moved down a row.
        '''
        # Get a list of every block that is available on the gameboard.
        blocks = get_rect_list(bricks)

        # A complete row, is a row with the same amount of blocks
        # as there is columns
        complete_row = self.cols

        # Something odd here. Cant right figure out the behaviour.
        # Thats why I Subtract with block_size in the y-axis pos.
        ruler_rect = Rect((self._pos_x, self._board.bottom - self.block_size),
                          (self._board.width, self.block_size))

        remove_list = []
        for row in range(0, self.rows):
            collision_indices = ruler_rect.collidelistall(blocks)
            if len(collision_indices) == complete_row:
                for index in collision_indices:
                    remove_list.append(blocks[index])
                return remove_list
            ruler_rect.y -= self.block_size

class Game(object):
    '''
    Game board, bricks and controls.
    '''
    def __init__(self, x, y, cols, rows):
        '''
        '''
        # Block setting
        self.block_size = 20

        self.width = cols * self.block_size
        self.height = rows * self.block_size
        self.gameboard = GameBoard((x, y), (cols, rows), self.block_size, (0x00, 0x00, 0x00))

        # Contains a list of previously active bricks.
        self.brick_list = []
        # Speed affects all moving objects, such as the active_brick
        self.speed = 1

        self.new_brick()

    def new_brick(self):
        '''
        Creates a new brick when previous get locked in place
        '''
        self.brick_list.append(Brick(random.randint(1, 2),
                                     self.gameboard.center(),
                                     self.block_size))
        self.active_brick = self.brick_list[-1]

    def draw(self):
        '''
        Draw game
        '''
        self.gameboard.draw()
        for brick in self.brick_list:
            brick.draw()

    def remove_complete_rows(self):
        '''
        For every complete row, request to remove it.
        The request is sent to each brick in the game.
        '''
        # Check if row/rows became complete
        while True:
            remove_list = self.gameboard.ruler(self.brick_list)
            if remove_list:
                for brick in self.brick_list:
                    brick.remove(remove_list)
            else:
                break


    def proceed(self):
        # Is brick active, if not, kill it and create a new one.
        if self.found_collision():
            self.active_brick.move_up(self.speed)
            self.new_brick()
            self.remove_complete_rows()
        else:
            self.active_brick.move_down(self.speed)

    def get_dead_rects(self):
        '''
        Returns a list of dead rects
        '''
        dead_bricks = copy.deepcopy(self.brick_list)
        dead_bricks.pop()
        dead_rects = get_rect_list(dead_bricks)
        return dead_rects

    def found_collision(self):
        if self.active_brick.gameboard_collision(self.gameboard.get_rect()):
            return True
        elif self.active_brick.shape_collision(self.get_dead_rects()):
            return True
        else:
            return False

    def controls(self, direction):
        '''
        Will be called for a given intervall. This prevents the blocks from
        flying from left to right in high speed
        '''

        # Check if user want to move brick side ways.
        if direction == 'right':
            self.active_brick.move_right()
        elif direction == 'left':
            self.active_brick.move_left()
        elif direction == 'down':
            self.active_brick.move_down(self.speed * 10)
        elif direction == 'pos_rotate':
            self.active_brick.pos_rotate()
        elif direction == 'neg_rotate':
            self.active_brick.neg_rotate()

        # Damage control after collision
        if self.found_collision():
            if direction == 'right':
                self.active_brick.move_left()
            elif direction == 'left':
                self.active_brick.move_right()
            elif direction == 'down':
                self.active_brick.move_up(self.speed * 10)
            elif direction == 'pos_rotate':
                self.active_brick.neg_rotate()
            elif direction == 'neg_rotate':
                self.active_brick.pos_rotate()


# Create a gamecontroller and a gameboard with a width and a height
# Width and height is given in columns and rows.
betris = Game(100, 50, 10, 20)

def control_tick():
    '''
    Gets triggerad by clock.
    Tells the game betris what the user want to do, if any.
    '''
    if keyboard.right:
        betris.controls('right')
    elif keyboard.left:
        betris.controls('left')
    elif keyboard.up:
        betris.controls('pos_rotate')
    elif keyboard.down:
        betris.controls('neg_rotate')
    elif keyboard.space:
        betris.controls('down')

# Setup time between each check of user controls.
clock.schedule_interval(control_tick, 0.05)

def update():
    '''
    Update block position.
    '''
    betris.proceed()

def draw():
    '''
    Draw game
    '''
    screen.fill((0xFF, 0xFF, 0xFF))
    betris.draw()


